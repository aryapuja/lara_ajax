<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="app">
        Nama: <input type="text" v-model="newName" > <br>
        Email: <input type="text" v-model="newEmail" > <br>
        Password: <input type="password" v-model="newPassword" > <br>
        <button @click="addUser" v-if="!editing"> submit </button>
        <button @click="editUsers" v-else="!editing"> Update </button>
        <ul>
            <li v-for="(user, index) in users">
                <span>@{{ user.id }} - @{{ user.name }} - @{{user.email}}</span>
                <button @click="delUser(index, user)"> X </button>
                <button @click="btnEdit(index, user)"> Edit </button>
            </li>
        </ul>
    </div>
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

<script>
    var app = new Vue({
        el: '#app',
        data : {
            editing: false,
            id_user: null,
            position: null,
            newName:"",
            newEmail:"",
            newPassword:"",
            users: []
        },
        methods:{
            addUser: function(){
                let inputName = this.newName.trim();
                let inputEmail = this.newEmail.trim();
                let inputPassword = this.newPassword.trim();
                if(inputName){
                    // POST /someUrl
                    this.$http.post('/api/user', {name: inputName, email: inputEmail, password: inputPassword}).then(response => {
                        this.users.push({name: inputName, email:inputEmail, password:inputPassword})
                    });
                };
            },
            delUser: function(index, user){
                // console.log(user.id);
                if(confirm("anda yakin?")){
                    this.$http.post('/api/user/delete/'+user.id).then(response => {
                        this.users.splice(index,1); // index utk hapus yg di web, id utk hapus yang di db
                    });

                }
            },
            btnEdit: function(index, user){
                this.editing= true;
                this.newName=this.users[index].name;
                this.newEmail=this.users[index].email;
                this.id_user = user.id;
                this.position = index;
            },
            editUsers: function(){
                let inputName = this.newName.trim();
                let inputEmail = this.newEmail.trim();
                let inputPassword = this.newPassword.trim();
                let id_user = this.id_user;
                if(inputName){
                    // POST /someUrl
                    this.$http.post('/api/user/update/'+id_user, {name: inputName, email: inputEmail, password: inputPassword}).then(response => {
                        // this.users.push({name: inputName, email:inputEmail, password:inputPassword})
                        this.users[this.position].name =inputName;
                        this.users[this.position].email =inputEmail;
                        this.editing= false;
                        this.id_user= null;
                        this.newName="";
                        this.newEmail="";
                        this.newPassword="";
                    });
                };
            }
        },
        mounted:function(){
            // GET /someUrl
            this.$http.get('/api/user').then(response => {
                // get body data
                // console.log(response.body)
                let result = response.body;
                this.users = result
            });
        }
    })
</script>
</body>
</html>