<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return $users;
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => encrypt($request->password)
        ]);
        return $user;
    }

    public function delete($id)
    {
        User::destroy($id);
        return 'success';
    }

    public function update($id, Request $request)
    {
        $newData = User::where('id', $id)->first();
        $newData->name  = $request->name;
        $newData->email  = $request->email;
        $newData->password  = encrypt($request->password);
        $newData->save();
        return 'success';

    }
}
